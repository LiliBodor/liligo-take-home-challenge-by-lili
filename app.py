import read
import write
import visualize
import psycopg2

def main():
    try:
        write.create_tables()
    except(Exception, psycopg2.Error) as error:
        print(error)
    finally:
        read.list_days()
        visualize.result_visualization()


main()
