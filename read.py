import mysql.connector as mysql
import psycopg2
import datetime
import write

def get_first_day():
    connection = mysql.connect(user='liligo', password='liligo', host='127.0.0.1', database='liligo')
    cursor = connection.cursor()

    first_day_query = "SELECT * FROM liligo.liligo ORDER BY timestamp ASC LIMIT 1"
    cursor.execute(first_day_query)

    result = cursor.fetchall()

    return result[0][1]

def get_last_day():
    connection = mysql.connect(user='liligo', password='liligo', host='127.0.0.1', database='liligo')
    cursor = connection.cursor()

    last_day_query = "SELECT * FROM liligo.liligo ORDER BY timestamp DESC LIMIT 1"
    cursor.execute(last_day_query)

    result = cursor.fetchall()

    return result[0][1]

def list_days():
    connection = mysql.connect(user='liligo', password='liligo', host='127.0.0.1', database='liligo')
    cursor = connection.cursor()

    connection_postgre = psycopg2.connect(database="liligo", user="liligo", password="liligo", host="127.0.0.1", port="5432")
    cursor_postgre = connection_postgre.cursor()
    day_query = "SELECT date FROM liligo"
    cursor_postgre.execute(day_query)
    day_query = cursor_postgre.fetchall()
    day_query = [day[0] for day in day_query]

    metric_types = ['page_view', 'click_button', 'click-out']
    start = get_first_day()
    final = get_last_day()
    final = datetime.date(final.year, final.month, final.day)
    start = datetime.date(start.year, start.month, start.day)
    next_day = datetime.date(start.year, start.month, start.day)

    query = "SELECT COUNT(*) FROM liligo.liligo WHERE timestamp LIKE %s AND metric = %s"

    day = (str)(start) + "%"
    index = 0
    metrics = []


    while next_day <= final:
        metrics.clear()
        print("---- ",  day, " ----")
        for metric in metric_types:
            cursor.execute(query, (day, metric))
            print("---- ",  metric, " ----")
            single_metric = cursor.fetchall()
            print(single_metric)
            metrics.append(int(single_metric[0][0]))
        if(next_day not in day_query):
            write.insert_recods(day, metrics)
        index += 1
        next_day = datetime.date(start.year, start.month, start.day)
        next_day = next_day + datetime.timedelta(days=index)
        day = datetime.date(next_day.year, next_day.month, next_day.day)
        day = (str)(next_day) + "%"

    connection.close()
