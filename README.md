# liligo-take-home-challenge-by-lili

First of all thank you for your patience and your time to see my soltion to the challenge. 

In this repository you can find the [lili's Analytics developer take-home challenge](https://gitlab.com/liligo/analytics-developer-take-home-challenge/tree/master) solution. 

In the liligo_take_home_challenge folder you can find 

     - the requirements.txt 
     - all the python files. 

The **requirements.txt** is a collector for all the libraries that are needed to run the application. 

**Read.py** and the **write.py** files are the solutions of the first task. The first one containes the the MySQL databese's data aggregation, the second one is responsible for cerating the tables and inserting the datas to the Postgres database. 

The **visualize.py** is for the results' visualization. And last bust not least the **app.py** is containes the main where all the magic happens. :)  

After you cloned the repository, all you can do is to use `pip install requirements.txt`. In the terminal window you have to see all the libraries and their version number, something like this: `matplotlib==3.1.0`.
Then you can the application by calling `python app.py`.
