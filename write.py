import psycopg2

def create_tables():

    connection = psycopg2.connect(database="liligo", user="liligo", password="liligo", host="127.0.0.1", port="5432")
    print("Opened Liligo Database Successfully")

    cursor = connection.cursor()
    cursor.execute('''CREATE TABLE liligo
           (DATE          DATE   NOT NULL,
           PAGE_VIEW            INT     NOT NULL,
           CLICK_BUTTON       INT     NOT NULL,
           CLICK_OUT       INT     NOT NULL);''')
    print("Table created successfully")

    connection.commit()
    connection.close()

def insert_recods(day, metrics):

    connection = psycopg2.connect(database="liligo", user="liligo", password="liligo", host="127.0.0.1", port="5432")
    print("Opened Liligo Database Successfully")

    cursor = connection.cursor()

    cursor.execute("INSERT INTO liligo (DATE,PAGE_VIEW,CLICK_BUTTON,CLICK_OUT) \
      VALUES (%s,%s,%s,%s)", (day, int(metrics[0]), int(metrics[1]), int(metrics[2])))


    connection.commit()
    print ("Records created successfully in LILIGO Table")
    connection.close()
