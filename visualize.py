import psycopg2
import numpy as np
import matplotlib.pyplot as plt

def result_visualization():

    connection= psycopg2.connect(database="liligo", user="liligo", password="liligo", host="127.0.0.1", port="5432")
    cursor = connection.cursor()

    page_view_query = 'SELECT page_view FROM liligo'
    cursor.execute(page_view_query)
    page_view_query = cursor.fetchall()

    click_button_query = "SELECT click_button FROM liligo "
    cursor.execute(click_button_query)
    click_button_query  = cursor.fetchall()

    click_out_query = "SELECT click_out FROM liligo "
    cursor.execute(click_out_query)
    click_out_query = cursor.fetchall()

    day_query = "SELECT date FROM liligo"
    cursor.execute(day_query)
    day_query = cursor.fetchall()

    day_query = [day[0] for day in day_query]
    page_view = [pv[0] for pv in page_view_query]
    click_button = [cb[0] for cb in click_button_query]
    click_out = [co[0] for co in click_out_query]

    days = len(day_query)
    index = np.arange(days)
    bar_width = 0.3
    opacity = 0.8

    ax = plt.subplot()
    first_column = plt.bar(index, page_view, bar_width, alpha=opacity, color='b', label='Page view')
    second_column = plt.bar(index + bar_width, click_button, bar_width, alpha=opacity, color='g', label='Click button')
    third_column = plt.bar(index + bar_width + bar_width, click_out, bar_width, alpha=opacity, color='m', label='Click out')
    plt.xlabel('Dates')
    plt.ylabel('Metrics')
    plt.title('Result')
    plt.xticks(index + bar_width, day_query)
    plt.legend()
    plt.show()
